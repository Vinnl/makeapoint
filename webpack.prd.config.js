var baseConfig = require('./webpack.common.config');

var webpack = require('webpack');
var path = require('path');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var OfflinePlugin = require('offline-plugin');

module.exports = function makeWebpackConfig(){
  var config = baseConfig;

  config.devtool = '#source-map';

  config.output = {
    filename: '[name].[hash].js',
    path: path.join(__dirname, './build'),
    publicPath: '/'
  };

  config.plugins.push(
    new CleanWebpackPlugin(['build']),

    // Reference: http://webpack.github.io/docs/list-of-plugins.html#dedupeplugin
    // Dedupe modules in the output
    new webpack.optimize.DedupePlugin(),

    // Reference: http://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
    // Minify all javascript, switch loaders to minimizing mode
    new webpack.optimize.UglifyJsPlugin(),

    // Reference: https://github.com/NekR/offline-plugin
    // Cache assets for offline use using Service Workers or, as a fallback, AppCache.
    new OfflinePlugin(),

    new webpack.DefinePlugin({
      'process.env.BABEL_ENV': 'production'
    })
  )

  return config;
}();
