var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');
var cssnext = require('postcss-cssnext');
var stylelint = require('stylelint');
var colorguard = require('colorguard');

module.exports = function makeWebpackConfig () {
  var config = {};

  config.devtool = '#source-map';

  config.entry = {
    app: [
      './src/app',
      './node_modules/normalize.css/normalize',
      './src/styles/styles',
      // Favicons, great shoutout to realfavicongenerator.net:
      './src/assets/apple-touch-icon.png',
      './src/assets/apple-touch-icon-precomposed.png',
      './src/assets/apple-touch-icon-57x57.png',
      './src/assets/apple-touch-icon-60x60.png',
      './src/assets/apple-touch-icon-72x72.png',
      './src/assets/apple-touch-icon-76x76.png',
      './src/assets/apple-touch-icon-114x114.png',
      './src/assets/apple-touch-icon-120x120.png',
      './src/assets/apple-touch-icon-144x144.png',
      './src/assets/apple-touch-icon-152x152.png',
      './src/assets/apple-touch-icon-180x180.png',
      './src/assets/favicon.ico',
      './src/assets/favicon-32x32.png',
      './src/assets/favicon-194x194.png',
      './src/assets/favicon-96x96.png',
      './src/assets/android-chrome-192x192.png',
      './src/assets/favicon-16x16.png',
      './src/assets/manifest.json',
      './src/assets/safari-pinned-tab.svg',
      './src/assets/mstile-70x70.png',
      './src/assets/mstile-144x144.png',
      './src/assets/mstile-150x150.png',
      './src/assets/mstile-310x310.png',
      './src/assets/mstile-310x150.png',
    ]
  };

  config.resolve = {
    extensions: ['', '.js', '.css'],
    root: ['src', 'node_modules']
  };

  config.module = {
    loaders: [
      {
        test: /\.js?$/,
        loader: 'babel',
        include: path.join(__dirname, 'src'),
        exclude: /node_modules/,
      },
      {
        test: /\.scss$/,
        loader: 'style-loader!css-loader!sass-loader?sourceMap',
        include: path.join(__dirname, 'src/styles'),
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader?sourceMap!postcss-loader',
        include: [
          path.join(__dirname, 'src/styles'),
          path.join(__dirname, 'node_modules/normalize.css/'),
        ],
      },
      {
        test: /\/assets\//,
        loader: 'file-loader?name=[path][name].[ext]&context=./src/assets',
        include: path.join(__dirname, 'src/assets'),
        exclude: /node_modules/,
      },
    ]
  };

  config.postcss = function () {
    return [stylelint, colorguard, cssnext];
  };

  config.plugins = [
    // Reference: http://webpack.github.io/docs/list-of-plugins.html#noerrorsplugin
    // Only emit files when there are no errors
    new webpack.NoErrorsPlugin(),

    // Reference: https://github.com/ampedandwired/html-webpack-plugin
    // Render index.html
    new HtmlWebpackPlugin({
      template: './index.html',
      inject: 'body',
    })
  ];

  return config;

}();
