var baseConfig = require('./webpack.common.config');

var webpack = require('webpack');
var path = require('path');

module.exports = function makeWebpackConfig () {
  var config = baseConfig;

  config.devtool = '#eval';

  config.entry.app.unshift(
    'webpack-dev-server/client?http://0.0.0.0:3000',
    'webpack/hot/dev-server'
  );

  config.output = {
    filename: '[name].js',
    path: path.join(__dirname, './build'),
    publicPath: '/'
  };

  config.plugins.push(new webpack.HotModuleReplacementPlugin());

  return config;

}();
