# To get set up
With `npm` installed, run `npm install`.

# To try it out
Run `npm start` to start a local server to check whether everything looks as expected.

# To build
`npm run build`

# To deploy
If you're Vincent, you can deploy to Amazon S3 by running `npm run deploy`
