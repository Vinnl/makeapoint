import Cycle from '@cycle/core';
import { makeDOMDriver } from '@cycle/dom';
import { makeHistoryDriver } from '@cycle/history';
import { createHistory, useQueries } from 'history'
import Location from './dialogue/components/location/main';
import MakeAPoint from './dialogue/components/makeapoint/main';

// Initialise caching the assets using a Service Worker or, as a fallback, AppCache.
// See https://github.com/NekR/offline-plugin
require('offline-plugin/runtime').install();

function main(drivers){
  return {
    DOM: MakeAPoint(drivers),
    History: Location(drivers),
  };
}

const drivers = {
  DOM: makeDOMDriver('#app'),
  History: makeHistoryDriver(useQueries(createHistory)()),
  Keypress: () => Rx.Observable.fromEvent(document, 'keypress')
};

Cycle.run(main, drivers);
