import Rx from 'rx';
import sinon from 'sinon';

function getEventStreamsForEvents(events){
  return Object.keys(events).reduce(
    (stub, eventName) => {
      let event = events[eventName];
      event.preventDefault = sinon.stub();

      return stub.withArgs(eventName).returns(Rx.Observable.just(event));
    },
    sinon.stub()
  );
}

function getEventStream(events){
  if(events){
    return getEventStreamsForEvents(events);
  }
  return () => Rx.Observable.never();
}

export default function createMockDomDriver(eventsForSelector = {}){
  return {
    select: (selector) => {
      return {
        events: getEventStream(eventsForSelector[selector]),
      };
    },
  };
}
