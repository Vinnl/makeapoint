import test from 'tape';
import Rx from 'rx';

import selectQueryMessage from './selectQueryMessage';

test('Select an empty message when no `point` was passed in the query string', (t) => {
  t.plan(1);

  const mockHistory$ = Rx.Observable.just({ query: {}});

  selectQueryMessage(mockHistory$).subscribe(msg => {
    t.equal(msg, '');
  });
});

test('Select the message when a `point` was passed in the query string', (t) => {
  t.plan(1);

  const mockHistory$ = Rx.Observable.just({ query: { point: 'Test message' }});

  selectQueryMessage(mockHistory$).subscribe(msg => {
    t.equal(msg, 'Test message');
  });
});
