import test from 'tape';
import Rx from 'rx';
import sinon from 'sinon';

import createMockDomDriver from './mockDomDriver.test';
import selectCurrentMessage from './selectCurrentMessage';

test('Select an empty message initially', (t) => {
  t.plan(1);

  const mockDomDriver$ = createMockDomDriver();
  const mockHistory$ = Rx.Observable.never();

  selectCurrentMessage(mockDomDriver$, mockHistory$).subscribe(msg => {
    t.equal(msg, '');
  });
});

test('Select the message from the query string as the current message', (t) => {
  t.plan(1);

  const mockDomDriver$ = createMockDomDriver();
  const mockHistory$ = Rx.Observable.never();
  selectCurrentMessage.__Rewire__(
    'selectQueryMessage',
    sinon.stub().returns(Rx.Observable.just('Query message test'))
  );

  selectCurrentMessage(mockDomDriver$, mockHistory$)
  .skip(1) // The form is initially empty
  .subscribe(msg => {
    t.equal(msg, 'Query message test');
  });

  selectCurrentMessage.__ResetDependency__('selectQueryMessage');
});

test('Do not replace the current message if the query string message is empty', (t) => {
  t.plan(1);

  const mockDomDriver$ = createMockDomDriver({
    '.setMessageForm__messageInput': {
      'input': { target: { value: 'Test message that should not be overridden' }}
    }
  });
  const mockHistory$ = Rx.Observable.never();
  selectCurrentMessage.__Rewire__(
    'selectQueryMessage',
    sinon.stub().returns(Rx.Observable.just(''))
  );

  selectCurrentMessage(mockDomDriver$, mockHistory$)
  .skip(1) // The form is initially empty
  .subscribe(msg => {
    t.equal(msg, 'Test message that should not be overridden');
  });

  selectCurrentMessage.__ResetDependency__('selectQueryMessage');
});

test('Select the latest input as current message', (t) => {
  t.plan(1);

  const mockDomDriver$ = createMockDomDriver({
    '.setMessageForm__messageInput': {
      'input': { target: { value: 'Test message' }}
    }
  });
  const mockHistory$ = Rx.Observable.never();

  selectCurrentMessage(mockDomDriver$, mockHistory$)
  .skip(1) // The form is initially empty
  .subscribe(msg => {
    t.equal(msg, 'Test message');
  });
});
