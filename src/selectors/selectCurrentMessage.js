import selectQueryMessage from './selectQueryMessage';

function selectCurrentMessage(domDriver, historyDriver){
  const currentInput$ = domDriver.select('.setMessageForm__messageInput')
                        .events('input').map(ev => ev.target.value);

  const currentQuery$ = selectQueryMessage(historyDriver);

  const currentMessage$ = currentInput$.merge(
    currentQuery$.filter(currentQuery => currentQuery.length > 0)
  )
  .startWith('');

  return currentMessage$;
}

export default selectCurrentMessage;
