import test from 'tape';
import Rx from 'rx';
import sinon from 'sinon';

import createMockDomDriver from './mockDomDriver.test';
import selectIsConfiguring from './selectIsConfiguring';

test('Be configuring after clicking the big-ass message', (t) => {
  t.plan(1);

  const mockDomDriver$ = createMockDomDriver({
    '.bigAssMessage': {'click': {}},
  });
  const mockHistoryDriver$ = Rx.Observable.never();
  const mockKeypressDriver$ = Rx.Observable.never();

  selectIsConfiguring(mockDomDriver$, mockHistoryDriver$, mockKeypressDriver$)
  .subscribe(isConfiguring => {
    t.equal(isConfiguring, true);
  });
});

test('Not configuring after clicking the submit button', (t) => {
  t.plan(1);

  const mockDomDriver$ = createMockDomDriver({
    '.setMessageForm__showButton': {'click': {}},
  });
  const mockHistoryDriver$ = Rx.Observable.never();
  const mockKeypressDriver$ = Rx.Observable.never();

  selectIsConfiguring(mockDomDriver$, mockHistoryDriver$, mockKeypressDriver$)
  .subscribe(isConfiguring => {
    t.equal(isConfiguring, false);
  });
});

test('Not configuring after submitting the configuration form', (t) => {
  t.plan(1);

  const mockDomDriver$ = createMockDomDriver({
    '.setMessageForm': {'submit': {}},
  });
  const mockHistoryDriver$ = Rx.Observable.never();
  const mockKeypressDriver$ = Rx.Observable.never();

  selectIsConfiguring(mockDomDriver$, mockHistoryDriver$, mockKeypressDriver$)
  .subscribe(isConfiguring => {
    t.equal(isConfiguring, false);
  });
});

test('Be configuring after pressing `Escape`', (t) => {
  t.plan(1);

  const mockDomDriver$ = createMockDomDriver();
  const mockHistoryDriver$ = Rx.Observable.never();
  const mockKeypressDriver$ = Rx.Observable.just({ keyCode: 27});

  selectIsConfiguring(mockDomDriver$, mockHistoryDriver$, mockKeypressDriver$)
  .subscribe(isConfiguring => {
    t.equal(isConfiguring, true);
  });
});

test('Be configuring when there\'s no point provided as a query parameter', (t) => {
  t.plan(1);

  const mockDomDriver$ = createMockDomDriver();
  const mockHistoryDriver$ = Rx.Observable.never();
  const mockKeypressDriver$ = Rx.Observable.never();
  selectIsConfiguring.__Rewire__(
    'selectQueryMessage',
    sinon.stub().returns(Rx.Observable.just(''))
  );

  selectIsConfiguring(mockDomDriver$, mockHistoryDriver$, mockKeypressDriver$)
  .subscribe(isConfiguring => {
    t.equal(isConfiguring, true);
  });

  selectIsConfiguring.__ResetDependency__('selectQueryMessage');
});

test('Not configuring when there\'s a point provided as a query parameter', (t) => {
  t.plan(1);

  const mockDomDriver$ = createMockDomDriver();
  const mockHistoryDriver$ = Rx.Observable.never();
  const mockKeypressDriver$ = Rx.Observable.never();
  selectIsConfiguring.__Rewire__(
    'selectQueryMessage',
    sinon.stub().returns(Rx.Observable.just('Test message'))
  );

  selectIsConfiguring(mockDomDriver$, mockHistoryDriver$, mockKeypressDriver$)
  .subscribe(isConfiguring => {
    t.equal(isConfiguring, false);
  });

  selectIsConfiguring.__ResetDependency__('selectQueryMessage');
});
