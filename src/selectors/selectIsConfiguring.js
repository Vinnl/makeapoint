import Rx from 'rx';

import selectQueryMessage from './selectQueryMessage';

function selectIsConfiguring(domDriver, historyDriver, keypressDriver){
  return Rx.Observable.merge(
    domDriver.select('.bigAssMessage')
      .events('click').map(ev => true),
    domDriver.select('.setMessageForm')
      .events('submit')
      .merge(domDriver.select('.setMessageForm__showButton').events('click'))
      .map(ev => {
        ev.preventDefault();
        return false;
      }),
    // When the user visits with the point set in the URL, skip the configuration screen
    selectQueryMessage(historyDriver)
      .map(currentQuery => currentQuery.length === 0),
    // Also exit point-making mode when the user presses Escape:
    keypressDriver
      .filter(ev => ev.keyCode === 27)
      .map(ev => true)
  );
}

export default selectIsConfiguring;
