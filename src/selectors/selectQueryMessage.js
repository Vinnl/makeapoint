function selectQueryMessage(history$){
  return history$
    .distinctUntilChanged(newLocation => newLocation.search)
    .map(location => location.query.point || '');
}

export default selectQueryMessage;
