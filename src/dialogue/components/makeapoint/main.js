import view from './view';
import intent from './intent';
import model from './model';

function main(drivers){
  const state$ = model(intent(drivers));

  return view(state$);
}

export default main;
