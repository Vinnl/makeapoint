import test from 'tape';
import Rx from 'rx';

import toHtml from 'vdom-to-html';
import cheerio from 'cheerio';

import view from './view';

test('Prefill the form with a previous message', (t) => {
  t.plan(2);

  const mockState$ = Rx.Observable.just({
    isConfiguring: true,
    currentMessage: 'Test previous message',
  });

  view(mockState$).subscribe(dom => {
    const $ = cheerio.load(toHtml(dom));
    t.equal($('.setMessageForm__messageInput').length, 1);
    // In `virtual-dom`, the `value` property is an object that,
    // in turn, also contains a `value` property containing the actual value.
    // Unfortunately, vdom-to-html apparently doesn't understand that.
    // Therefore, it will render `[Object object]` instead of `Test previous message`:
    // t.equal($('.setMessageForm__messageInput').attr('value'), 'Test previous message');
    // Instead, we can do this, which is less clean but works:
    t.equal(dom.children[0].properties.value.value, 'Test previous message');
  });
});

test('Include the given message in the Twitter link', (t) => {
  t.plan(2);

  const mockState$ = Rx.Observable.just({
    isConfiguring: true,
    currentMessage: 'Test message',
  });

  view(mockState$).subscribe(dom => {
    const $ = cheerio.load(toHtml(dom));
    t.equal($('.setMessageForm__shareButton__twitter').length, 1);
    t.equal(
      $('.setMessageForm__shareButton__twitter').attr('href'),
      'https://twitter.com/intent/tweet?text=https%3A%2F%2FMakeAPoint.VincentTunru.com%2F%3Fpoint%3DTest%20message'
    );
  });
});

test('Include the given message in the Facebook link', (t) => {
  t.plan(2);

  const mockState$ = Rx.Observable.just({
    isConfiguring: true,
    currentMessage: 'Test message',
  });

  view(mockState$).subscribe(dom => {
    const $ = cheerio.load(toHtml(dom));
    t.equal($('.setMessageForm__shareButton__facebook').length, 1);
    t.equal(
      $('.setMessageForm__shareButton__facebook').attr('href'),
      'https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2FMakeAPoint.VincentTunru.com%2F%3Fpoint%3DTest%20message'
    );
  });
});

test('Include the given message in the reddit link', (t) => {
  t.plan(2);

  const mockState$ = Rx.Observable.just({
    isConfiguring: true,
    currentMessage: 'Test message',
  });

  view(mockState$).subscribe(dom => {
    const $ = cheerio.load(toHtml(dom));
    t.equal($('.setMessageForm__shareButton__reddit').length, 1);
    t.equal(
      $('.setMessageForm__shareButton__reddit').attr('href'),
      'https://reddit.com/submit/?url=https%3A%2F%2FMakeAPoint.VincentTunru.com%2F%3Fpoint%3DTest%20message'
    );
  });
});

test('Link back to my website', (t) => {
  t.plan(2);

  const mockState$ = Rx.Observable.just({
    isConfiguring: true,
    currentMessage: 'Test message',
  });

  view(mockState$).subscribe(dom => {
    const $ = cheerio.load(toHtml(dom));
    t.equal($('.attribution a').length, 1);
    t.equal(
      $('.attribution a').attr('href'),
      'https://VincentTunru.com'
    );
  });
});

test('Displaying a message', (t) => {
  t.plan(2);

  const mockState$ = Rx.Observable.just({
    isConfiguring: false,
    currentMessage: 'Test message',
  });

  view(mockState$).subscribe(dom => {
    t.equal(dom.children.length, 1 );
    t.equal(dom.children[0].text, 'Test message' );
  });
});
