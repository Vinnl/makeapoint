import test from 'tape';
import sinon from 'sinon';

import intent from './intent';

const mockDrivers = {
  DOM: 'Mock DOM driver',
  History: 'Mock History driver',
  Keypress: 'Mock Keypress driver',
};

test((t) => {
  t.plan(2);

  intent.__Rewire__('selectIsConfiguring', sinon.stub().returns('isConfiguring stream'));
  intent.__Rewire__('selectCurrentMessage', sinon.stub().returns('currentMessage stream'));

  const result = intent(mockDrivers);
  t.equal(result.isConfiguring$, 'isConfiguring stream');
  t.equal(result.currentMessage$, 'currentMessage stream');

  intent.__ResetDependency__('selectIsConfiguring');
  intent.__ResetDependency__('selectCurrentMessage');
});
