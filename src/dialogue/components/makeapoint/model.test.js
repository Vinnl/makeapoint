import test from 'tape';
import Rx from 'rx';

import model from './model';

test((t) => {
  t.plan(2);

  const mockActions = {
    isConfiguring$: Rx.Observable.just(false),
    currentMessage$: Rx.Observable.just('Don\'t forget to bring your towel!'),
  };

  model(mockActions).subscribe(({isConfiguring, currentMessage}) => {
    t.equal(isConfiguring, false );
    t.equal(currentMessage, 'Don\'t forget to bring your towel!');
  });
});
