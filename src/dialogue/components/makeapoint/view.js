import { form, label, input, a, p, small, aside, address } from '@cycle/dom';
import SocialIcons from '../../../socialIcons';

function renderMessage(message){
  return p('.bigAssMessage', message);
}

function renderShareButtons(message){
  return aside ([
    a({
      href: 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(`https://MakeAPoint.VincentTunru.com/?point=${message}`),
      target: '_blank',
      className: 'setMessageForm__shareButton setMessageForm__shareButton__facebook',
      title: 'Make your point on Facebook',
    }, SocialIcons.facebook),
    a({
      href: 'https://twitter.com/intent/tweet?text=' + encodeURIComponent(`https://MakeAPoint.VincentTunru.com/?point=${message}`),
      target: '_blank',
      className: 'setMessageForm__shareButton setMessageForm__shareButton__twitter',
      title: 'Make your point on Twitter',
    }, SocialIcons.twitter),
    a({
      href: 'https://reddit.com/submit/?url=' + encodeURIComponent(`https://MakeAPoint.VincentTunru.com/?point=${message}`),
      target: '_blank',
      className: 'setMessageForm__shareButton setMessageForm__shareButton__reddit',
      title: 'Make your point on Reddit',
    }, SocialIcons.reddit),
    address('.attribution', [
      'By ',
      a({
        href: 'https://VincentTunru.com',
        target: '_blank',
        className: 'link',
      }, 'Vincent Tunru')
    ])
  ]);
}

function renderConfigurationForm(message){
  return form('.setMessageForm', [
    input({
      type: 'text',
      className: 'setMessageForm__messageInput',
      id: 'setMessageForm__messageInput',
      value: message,
      placeholder: 'What\'s your point?',
      autofocus: 'autofocus',
      required: 'required',
    }),
    label(
      {
        className: 'setMessageForm__messageLabel',
        htmlFor: 'setMessageForm__messageInput',
      },
      'What\'s your point?'
    ),
    a({
      href: '?point=' + encodeURIComponent(message),
      className: 'setMessageForm__showButton',
    }, 'Make a Point!'),
    small('.setMessageForm__warning', '(Warning: not for epileptics)'),
    renderShareButtons(message)
  ]);
}

function view(state$){
  return state$.map(
    ({ isConfiguring, currentMessage }) => {
      if(isConfiguring){
        return renderConfigurationForm(currentMessage);
      } else {
        return renderMessage(currentMessage);
      }
    }
  );
}

export default view;
