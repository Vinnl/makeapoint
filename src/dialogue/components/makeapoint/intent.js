import selectIsConfiguring from '../../../selectors/selectIsConfiguring';
import selectCurrentMessage from '../../../selectors/selectCurrentMessage';

function intent(drivers){
  return {
    isConfiguring$: selectIsConfiguring(drivers.DOM, drivers.History, drivers.Keypress),
    currentMessage$: selectCurrentMessage(drivers.DOM, drivers.History),
  };
}

export default intent;
