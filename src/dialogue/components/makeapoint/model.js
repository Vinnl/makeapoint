import Rx from 'rx';

function model(actions){
  return Rx.Observable.combineLatest(actions.isConfiguring$, actions.currentMessage$,
    (isConfiguring, currentMessage) => {
      return { isConfiguring, currentMessage };
    }
  );
}

export default model;
