import test from 'tape';
import Rx from 'rx';
import sinon from 'sinon';

import main from './main';

test('Include the current message in the location when it is shown', (t) => {
  t.plan(1);

  const mockDrivers = {
    DOM: {},
    History: {},
    Keypress: {},
  };
  main.__Rewire__(
    'selectIsConfiguring',
    sinon.stub().returns(Rx.Observable.just(false))
  );
  main.__Rewire__(
    'selectCurrentMessage',
    sinon.stub().returns(Rx.Observable.just('Test message'))
  );

  main(mockDrivers)
  .subscribe(msg => {
    t.equal(msg, '?point=Test%20message');
  });

  main.__ResetDependency__('selectIsConfiguring');
  main.__ResetDependency__('selectCurrentMessage');
});

test('Do not include the current message in the location when the user is configuring', (t) => {
  t.plan(1);

  const mockDrivers = {
    DOM: {},
    History: {},
    Keypress: {},
  };
  main.__Rewire__(
    'selectIsConfiguring',
    sinon.stub().returns(Rx.Observable.just(true))
  );
  main.__Rewire__(
    'selectCurrentMessage',
    sinon.stub().returns(Rx.Observable.just('Test message'))
  );

  main(mockDrivers)
  .subscribe(msg => {
    t.equal(msg, '');
  });

  main.__ResetDependency__('selectIsConfiguring');
  main.__ResetDependency__('selectCurrentMessage');
});
