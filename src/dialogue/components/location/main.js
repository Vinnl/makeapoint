import selectIsConfiguring from '../../../selectors/selectIsConfiguring';
import selectCurrentMessage from '../../../selectors/selectCurrentMessage';

function intent(drivers){
  return {
    isConfiguring$: selectIsConfiguring(drivers.DOM, drivers.History, drivers.Keypress),
    currentMessage$: selectCurrentMessage(drivers.DOM, drivers.History),
  };
}

function model(actions){
  return actions
    .isConfiguring$
    .withLatestFrom(
      actions.currentMessage$,
      (isConfiguring, currentMessage) => {
        return { isConfiguring, currentMessage };
      }
    );
}

function view(state$){
  return state$.map(
    ({isConfiguring, currentMessage}) => {
      return isConfiguring ? '' : '?point=' + encodeURIComponent(currentMessage);
    }
  );
}

function main(drivers){
  const state$ = model(intent(drivers));

  return view(state$);
}

export default main;
